﻿using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Giact.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();

            // interface implements
            services.AddConfigurationService<GiactConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);

            // Configuration factory
            services.AddTransient(p =>
            {
                var configuration = p.GetService<IConfigurationService<GiactConfiguration>>().Get();
                configuration.ProxyUrl = $"http://{Settings.TlsProxy.Host}:{Settings.TlsProxy.Port}";
                return configuration as IGiactConfiguration;
            });

            services.AddTransient<Proxy.IGiactProxy, Proxy.GiactProxy>();
            services.AddTransient<IGiactService, GiactService>();

            // eventhub factory
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });
        }

       

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
           
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
        }
    }
}