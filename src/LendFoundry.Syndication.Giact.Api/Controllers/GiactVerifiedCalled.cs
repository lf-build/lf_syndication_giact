using LendFoundry.Syndication.Giact.Response;

namespace LendFoundry.Syndication.Giact.Api.Controllers
{
    public class GiactVerifiedCalled
    {
        public GiactVerifiedCalled()
        {
        }

        public GiactVerifiedCalled(IBankAccountVerificationResponse bankAccountVerificationResponse)
        {
            BankAccountVerificationResponse = bankAccountVerificationResponse;
        }
        public IBankAccountVerificationResponse BankAccountVerificationResponse { get; set; }
    }
}