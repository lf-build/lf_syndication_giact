﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Giact.Request;
using LendFoundry.SyndicationStore.Events;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Syndication.Giact.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IGiactService service, ILogger logger, IEventHubClient eventHubClient) : base(logger)
        {
            Service = service;
            EventHubClient = eventHubClient;
        }

        private IGiactService Service { get; }

        private IEventHubClient EventHubClient { get; }

        [HttpPost("{entityType}/{entityId}/bankaccount/verify")]
        public async Task<IActionResult> Verify([FromRoute] string entityType, [FromRoute] string entityId, [FromBody] VerifyBankAccountRequest request)
        {
            return await ExecuteAsync(async () =>
           {
               try
               {
                   var verificationResult = await Service.VerifyBankAccount(entityType, entityId, request);

                    await EventHubClient.Publish(nameof(SyndicationCalledEvent), new SyndicationCalledEvent
                    {
                        Data = verificationResult,
                        EntityId = entityId,
                        EntityType = entityType,
                        Name = "Giact"
                    });

                   await EventHubClient.Publish(new GiactVerifiedCalled(verificationResult));

                   return new OkObjectResult(verificationResult);
               }
               catch (GiactException ex)
               {
                    await EventHubClient.Publish(nameof(SyndicationCalledEvent), new SyndicationCalledEvent
                    {
                        Data = ex,
                        EntityId = entityId,
                        EntityType = entityType,
                        Name = "Giact"
                    });

                   return BadRequest(new Error(ex.Message));
               }
               catch (Exception ex)
               {
                   if (ex is ArgumentNullException || ex is ArgumentException )
                   {
                       Logger.Error(ex.Message);
                       throw;
                   }

                   var message = ex.Message;
                   var exception = ex.InnerException as GiactException;

                   if (exception != null)
                   {
                        message = exception.Message;
                        await EventHubClient.Publish(nameof(SyndicationCalledEvent), new SyndicationCalledEvent
                        {
                            Data = ex,
                            EntityId = entityId,
                            EntityType = entityType,
                            Name = "Giact"
                        });
                   }

                   Logger.Error($"The method VerifyBankAccount({entityType},{entityId}) raised an error: {message}", ex);
                   throw new NotFoundException($"Information not found for entity type :{entityType} with {entityId}");
               }
           });
        }

        private static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return $"{char.ToUpper(s[0])}{s.Substring(1)}";
        }
    }
}