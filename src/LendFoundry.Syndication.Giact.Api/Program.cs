﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;

namespace LendFoundry.Syndication.Giact.Api
{
    /// <summary>
    /// Entrypoint
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Entrypoint
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .Start("http://*:5000");

            using (host)
            {
                Console.WriteLine("Use Ctrl-C to shutdown the host...");
                host.WaitForShutdown();
            }
        }
    }
}
