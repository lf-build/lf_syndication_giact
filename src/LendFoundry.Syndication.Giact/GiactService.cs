﻿using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Giact.Proxy;
using LendFoundry.Syndication.Giact.Proxy.PostInquiryRequest;
using LendFoundry.Syndication.Giact.Request;
using LendFoundry.Syndication.Giact.Response;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Giact
{
    public class GiactService : IGiactService
    {
        public GiactService(IGiactProxy giactProxy, IGiactConfiguration configuration, ILookupService lookup)
        {
            if (giactProxy == null)
                throw new ArgumentNullException(nameof(giactProxy));
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (lookup == null)
                throw new ArgumentNullException(nameof(lookup));
            GiactProxy = giactProxy;
            Configuration = configuration;
            Lookup = lookup;
        }

        private IGiactProxy GiactProxy { get; }
        private IGiactConfiguration Configuration { get; }
        private ILookupService Lookup { get; }

        
        public Task<IBankAccountVerificationResponse> VerifyBankAccount(string entityType, string entityId, IVerifyBankAccountRequest request)
        {
            EnsureEntityId(entityId);
            entityType = EnsureEntityType(entityType);
            EnsureBankAccountInformation(request);
            if (string.IsNullOrEmpty(request.BusinessName))
            {
                if (string.IsNullOrEmpty(request.FirstName))
                    throw new InvalidArgumentException("FirstName and LastName or BusinessName is required");
                if (string.IsNullOrEmpty(request.LastName))
                    throw new InvalidArgumentException("FirstName and LastName or BusinessName is required");
            }

            if(string.IsNullOrEmpty(request.FirstName) && string.IsNullOrEmpty(request.LastName))
            {
                if(string.IsNullOrEmpty(request.BusinessName))
                    throw new InvalidArgumentException("FirstName and LastName or BusinessName is required");

            }
            var giactRequest = new GiactRequest
            {
                AccountNumber = request.AccountNumber,

                RoutingNumber = request.RoutingNumber,
                AccountType = request.AccountType,
                BusinessName = request.BusinessName,
                FirstName = request.FirstName,
                LastName = request.LastName
            };
            var inquiryRequest = new InquiryRequest(giactRequest);

            return Task.Run<IBankAccountVerificationResponse>(() => new BankAccountVerificationResponse(GiactProxy.PostInquiry(inquiryRequest)));
        }
        private void EnsureBankAccountInformation(IVerifyBankAccountRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (string.IsNullOrEmpty(request.AccountNumber))
                throw new ArgumentException("Account Number is required", nameof(request.AccountNumber));
            if (string.IsNullOrEmpty(request.RoutingNumber))
                throw new ArgumentException("Routing Number is required", nameof(request.RoutingNumber));
            if (request.AccountNumber.Equals(request.RoutingNumber))
                throw new ArgumentException("Account Number and Routing Number can't be same");
           
        }

        private void EnsureEntityId(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is required.");
        }

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}