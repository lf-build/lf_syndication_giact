﻿namespace LendFoundry.Syndication.Giact
{
    public class GiactRequest : IGiactRequest
    {
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
        public string CheckNumber { get; set; }
        public decimal? CheckAmount { get; set; }
        public BankAccountType AccountType { get; set; }
        public string NamePrefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string NameSuffix { get; set; }
        public string BusinessName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string TaxId { get; set; }
        public System.DateTime? DateOfBirth { get; set; }
        public string DlNumber { get; set; }
        public string DlState { get; set; }
        public string EmailAddress { get; set; }
        public string CurrentIpAddress { get; set; }
        public long? MobileConsentRecordId { get; set; }
        public AlternateIdType? AltIdType { get; set; }
        public string AltIdIssuer { get; set; }
        public string AltIdNumber { get; set; }
    }
}