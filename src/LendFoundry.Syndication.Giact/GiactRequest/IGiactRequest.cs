﻿namespace LendFoundry.Syndication.Giact
{
    public interface IGiactRequest
    {
        string RoutingNumber { get; set; }
        string AccountNumber { get; set; }
        string CheckNumber { get; set; }
        decimal? CheckAmount { get; set; }
        BankAccountType AccountType { get; set; }
        string NamePrefix { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string LastName { get; set; }
        string NameSuffix { get; set; }
        string BusinessName { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        string Country { get; set; }
        string PhoneNumber { get; set; }
        string TaxId { get; set; }
        System.DateTime? DateOfBirth { get; set; }
    }
}