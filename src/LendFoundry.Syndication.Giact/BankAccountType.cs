﻿namespace LendFoundry.Syndication.Giact
{
    public enum BankAccountType
    {
        /// <remarks/>
        Checking,

        /// <remarks/>
        Savings,

        /// <remarks/>
        Other,
    }
}