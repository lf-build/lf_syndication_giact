﻿using LendFoundry.Syndication.Giact.Proxy.PostInquiryResponse;
using LendFoundry.Syndication.Giact.ServiceReference;
namespace LendFoundry.Syndication.Giact.Response
{
    public class BankAccountVerificationResponse : IBankAccountVerificationResponse
    {
        public BankAccountVerificationResponse()
        {
        }

        public BankAccountVerificationResponse(InquiryResponse inquiryResponse)
        {
            if (inquiryResponse?.PostInquiryResult != null)
            {
                var postInquiryResult = inquiryResponse?.PostInquiryResult;
                ItemReferenceId = postInquiryResult.ItemReferenceId;
                CreatedDate = postInquiryResult.CreatedDate;

                VerificationResponse = postInquiryResult.VerificationResponse;
                AccountResponseCode = postInquiryResult.AccountResponseCode;
                BankName = postInquiryResult.BankName;
                AccountAddedDate = postInquiryResult.AccountAddedDate;
                AccountLastUpdatedDate = postInquiryResult.AccountLastUpdatedDate;
                AccountClosedDate = postInquiryResult.AccountClosedDate;
                if (postInquiryResult.FundsConfirmationResult != null)
                    FundsConfirmationResult = (FundsConfirmationResult)postInquiryResult.FundsConfirmationResult;
                CustomerResponseCode = postInquiryResult.CustomerResponseCode;
            }
        }

        public string ItemReferenceId { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public string VerificationResponse { get; set; }

        public string AccountResponseCode { get; set; }

        public string BankName { get; set; }

        public System.DateTime? AccountAddedDate { get; set; }

        public System.DateTime? AccountLastUpdatedDate { get; set; }
        public System.DateTime? AccountClosedDate { get; set; }

        public FundsConfirmationResult? FundsConfirmationResult { get; set; }
        public string CustomerResponseCode { get; set; }
    }
}