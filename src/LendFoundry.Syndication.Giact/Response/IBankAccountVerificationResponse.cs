﻿using LendFoundry.Syndication.Giact.ServiceReference;
namespace LendFoundry.Syndication.Giact.Response
{
    public interface IBankAccountVerificationResponse
    {
        string ItemReferenceId { get; set; }

        System.DateTime CreatedDate { get; set; }

        string VerificationResponse { get; set; }

        string AccountResponseCode { get; set; }

        string BankName { get; set; }

        System.DateTime? AccountAddedDate { get; set; }

        System.DateTime? AccountLastUpdatedDate { get; set; }
        System.DateTime? AccountClosedDate { get; set; }

        string CustomerResponseCode { get; set; }
        FundsConfirmationResult? FundsConfirmationResult { get; set; }
    }
}