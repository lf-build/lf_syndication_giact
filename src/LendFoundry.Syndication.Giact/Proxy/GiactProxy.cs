﻿using LendFoundry.Syndication.Giact.Proxy.PostInquiryResponse;
using RestSharp;
using System;

namespace LendFoundry.Syndication.Giact.Proxy
{
    public class GiactProxy : IGiactProxy
    {
        public GiactProxy(IGiactConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (string.IsNullOrEmpty(configuration.BaseUrl))
                throw new ArgumentNullException(nameof(configuration.BaseUrl));
            if (string.IsNullOrEmpty(configuration.UserName))
                throw new ArgumentNullException(nameof(configuration.UserName));
            if (string.IsNullOrEmpty(configuration.Password))
                throw new ArgumentNullException(nameof(configuration.Password));
            if (string.IsNullOrEmpty(configuration.ProxyUrl))
                throw new ArgumentNullException(nameof(configuration.ProxyUrl));
            Configuration = new GiactConfiguration(configuration);
        }

        public IGiactConfiguration Configuration { get; }

        public InquiryResponse PostInquiry(PostInquiryRequest.InquiryRequest inquiryRequest)
        {
            if (inquiryRequest == null)
                throw new ArgumentNullException(nameof(inquiryRequest));

            var baseUri = new Uri(Configuration.BaseUrl);

            var restRequest = new RestRequest(Method.POST);
            restRequest.AddHeader("content-type", "text/xml");

            var uri = new Uri($"{Configuration.ProxyUrl}{baseUri.PathAndQuery}");
            RestClient client = new RestClient(uri);
            restRequest.AddHeader("Host", baseUri.Host);

            var inquiryRequestXml = XmlSerialization.SerailizeObject(inquiryRequest, true);

            var request = $@"<?xml version=""1.0"" encoding=""utf-8""?><soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope""><soap12:Header><AuthenticationHeader xmlns=""http://api.giact.com/verificationservices/v5""><ApiUsername>{Configuration.UserName}</ApiUsername><ApiPassword>{Configuration.Password}</ApiPassword></AuthenticationHeader></soap12:Header><soap12:Body>
            {inquiryRequestXml}
            </soap12:Body></soap12:Envelope>";
            InquiryResponse inquiryResponse = null;
            restRequest.AddParameter("text/xml", request, ParameterType.RequestBody);

            var objectResponse = ExecuteRequest<Envelope>(client, restRequest);
            if (objectResponse?.Body?.PostInquiryResponse != null)
                inquiryResponse = objectResponse?.Body?.PostInquiryResponse;
            return inquiryResponse;
        }

        private static T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            var response = client.Execute(request);

            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new GiactException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new GiactException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if ((response.StatusCode.ToString().ToLower() == "internalservererror") || (response.Content.ToLower().Contains("error message:")))
                    throw new GiactException(
                      $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");
              
            }

            return XmlSerialization.DeserializeObject<T>(response.Content);
        }
    }
}