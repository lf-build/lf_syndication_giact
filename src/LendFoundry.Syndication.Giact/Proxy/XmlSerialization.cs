﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Giact.Proxy
{
    public sealed class ExtentedStringWriter : StringWriter
    {
        public ExtentedStringWriter(Encoding desiredEncoding)
        {
            Encoding = desiredEncoding;
        }

        public override Encoding Encoding { get; }
    }

    public static class XmlSerialization
    {
        public static string SerailizeObject<T>(T objectRequest, bool isOmitXmlDeclaration = false)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = isOmitXmlDeclaration;
            return SerializeObject(objectRequest, settings);
        }

        private static string SerializeObject<T>(T objectRequest, XmlWriterSettings settings)
        {
            string xmlSerialize;
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (ExtentedStringWriter sww = new ExtentedStringWriter(Encoding.UTF8))
            using (XmlWriter writer = XmlWriter.Create(sww, settings))
            {
                xmlSerializer.Serialize(writer, objectRequest);
                xmlSerialize = sww.ToString();
            }
            return xmlSerialize;
        }

        public static T DeserializeObject<T>(string input)
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
                return (T)ser.Deserialize(sr);
        }
    }
}