﻿using LendFoundry.Syndication.Giact.Proxy.PostInquiryResponse;

namespace LendFoundry.Syndication.Giact.Proxy
{
    public interface IGiactProxy
    {
        InquiryResponse PostInquiry(PostInquiryRequest.InquiryRequest inquiryRequest);
    }
}