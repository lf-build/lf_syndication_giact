﻿/*
 Licensed under the Apache License, Version 2.0

 http://www.apache.org/licenses/LICENSE-2.0
 */

using LendFoundry.Syndication.Giact.ServiceReference;
using System;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Giact.Proxy.PostInquiryRequest
{
    [XmlRoot("PostInquiry", Namespace = "http://api.giact.com/verificationservices/v5")]
    public class InquiryRequest
    {
        public InquiryRequest()
        {
        }

        public InquiryRequest(IGiactRequest request)
        {
            if (request != null)
            {
                Inquiry = new GiactInquiry_5_7
                {
                    UniqueId = Guid.NewGuid().ToString(),
                    Check = new ServiceReference.CheckInformation_5_7
                    {
                        AccountNumber = request.AccountNumber,
                        AccountType = (ServiceReference.BankAccountType)(int)request.AccountType,
                        CheckAmount = request.CheckAmount,
                        CheckNumber = request.CheckNumber,
                        RoutingNumber = request.RoutingNumber
                    },
                    Customer = new ServiceReference.CustomerInformation_5_7
                    {
                        AddressLine1 = request.AddressLine1,
                        AddressLine2 = request.AddressLine2,
                        BusinessName = request.BusinessName,
                        City = request.City,
                        Country = request.Country,
                        DateOfBirth = request.DateOfBirth,
                        FirstName = request.FirstName,
                        LastName = request.LastName,
                        MiddleName = request.MiddleName,
                        NamePrefix = request.NamePrefix,
                        NameSuffix = request.NameSuffix,
                        PhoneNumber = request.PhoneNumber,
                        State = request.State,
                        TaxId = request.TaxId,
                        ZipCode = request.ZipCode                        
                                                
                    },
                    GAuthenticateEnabled = true
                };
            }
        }

        [XmlElement("Inquiry")]
        public GiactInquiry_5_7 Inquiry { get; set; }
    }
}