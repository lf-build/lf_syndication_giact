﻿using LendFoundry.Syndication.Giact.ServiceReference;
using System;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Giact.Proxy.PostInquiryResponse
{
    [XmlRoot(ElementName = "PostInquiryResult", Namespace = "http://api.giact.com/verificationservices/v5")]
    public class PostInquiryResult
    {
        [XmlElement(ElementName = "ItemReferenceId", Namespace = "http://api.giact.com/verificationservices/v5")]
        public string ItemReferenceId { get; set; }

        [XmlElement(ElementName = "CreatedDate", Namespace = "http://api.giact.com/verificationservices/v5")]
        public DateTime CreatedDate { get; set; }

        [XmlElement(ElementName = "VerificationResponse", Namespace = "http://api.giact.com/verificationservices/v5")]
        public string VerificationResponse { get; set; }

        [XmlElement(ElementName = "AccountResponseCode", Namespace = "http://api.giact.com/verificationservices/v5")]
        public string AccountResponseCode { get; set; }

        [XmlElement(ElementName = "BankName", Namespace = "http://api.giact.com/verificationservices/v5")]
        public string BankName { get; set; }

        [XmlElement(ElementName = "AccountAddedDate", Namespace = "http://api.giact.com/verificationservices/v5")]
        public DateTime? AccountAddedDate { get; set; }

        [XmlElement(ElementName = "AccountLastUpdatedDate", Namespace = "http://api.giact.com/verificationservices/v5")]
        public DateTime? AccountLastUpdatedDate { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public DateTime? AccountClosedDate { get; set; }

        [XmlElement(ElementName = "FundsConfirmationResult", Namespace = "http://api.giact.com/verificationservices/v5")]
        public FundsConfirmationResult? FundsConfirmationResult { get; set; }

        [XmlElement(ElementName = "CustomerResponseCode", Namespace = "http://api.giact.com/verificationservices/v5")]
        public string CustomerResponseCode { get; set; }
    }

    [XmlRoot(ElementName = "PostInquiryResponse", Namespace = "http://api.giact.com/verificationservices/v5")]
    public class InquiryResponse
    {
        [XmlElement(ElementName = "PostInquiryResult", Namespace = "http://api.giact.com/verificationservices/v5")]
        public PostInquiryResult PostInquiryResult { get; set; }

        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class Body
    {
        [XmlElement(ElementName = "PostInquiryResponse", Namespace = "http://api.giact.com/verificationservices/v5")]
        public InquiryResponse PostInquiryResponse { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class Envelope
    {
        [XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
        public Body Body { get; set; }

        [XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soap { get; set; }

        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }

        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsd { get; set; }
    }
}