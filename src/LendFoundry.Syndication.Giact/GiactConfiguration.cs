﻿namespace LendFoundry.Syndication.Giact
{
    public class GiactConfiguration : IGiactConfiguration
    {
        public GiactConfiguration()
        { }

        public GiactConfiguration(IGiactConfiguration configuration)
        {
            if (configuration == null)
                return;
            BaseUrl = configuration.BaseUrl;
            UserName = configuration.UserName;
            Password = configuration.Password;
            ProxyUrl = configuration.ProxyUrl;
        }

        public string BaseUrl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ProxyUrl { get; set; }
    }
}