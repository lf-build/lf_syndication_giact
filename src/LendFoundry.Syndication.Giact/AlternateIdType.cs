﻿namespace LendFoundry.Syndication.Giact
{
    public enum AlternateIdType
    {
        /// <remarks/>
        UsaMilitaryId,

        /// <remarks/>
        UsaStateId,

        /// <remarks/>
        PassportUsa,

        /// <remarks/>
        PassportForeign,

        /// <remarks/>
        UsaResidentAlienId,

        /// <remarks/>
        StudentId,

        /// <remarks/>
        TribalId,

        /// <remarks/>
        DlCanada,

        /// <remarks/>
        DlMexico,

        /// <remarks/>
        OtherIdForeign,
    }
}