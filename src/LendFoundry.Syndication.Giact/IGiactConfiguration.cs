﻿namespace LendFoundry.Syndication.Giact
{
    public interface IGiactConfiguration
    {
        string BaseUrl { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string ProxyUrl { get; set; }
    }
}