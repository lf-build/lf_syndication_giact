﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.Giact
{
    [Serializable]
    public class GiactException : Exception
    {
        public string ErrorCode { get; set; }

        public GiactException()
        {
        }

        public GiactException(string message) : this(null, message, null)
        {
        }

        public GiactException(string errorCode, string message) : this(errorCode, message, null)
        {
        }

        public GiactException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }

        public GiactException(string message, Exception innerException) : this(null, message, innerException)
        {
        }

        protected GiactException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}