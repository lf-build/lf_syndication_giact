﻿namespace LendFoundry.Syndication.Giact.Request
{
    public interface IVerifyBankAccountRequest
    {
        string RoutingNumber { get; set; }
        string AccountNumber { get; set; }

        BankAccountType AccountType { get; set; }
       string FirstName { get; set; }
        string LastName { get; set; }
        string BusinessName { get; set; }
       

    }
}