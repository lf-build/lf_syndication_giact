﻿namespace LendFoundry.Syndication.Giact.Request
{
    public class VerifyBankAccountRequest : IVerifyBankAccountRequest
    {
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }

        public BankAccountType AccountType { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BusinessName { get; set; }

    }
}