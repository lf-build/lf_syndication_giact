﻿using LendFoundry.Syndication.Giact.Request;
using LendFoundry.Syndication.Giact.Response;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Giact
{
    public interface IGiactService
    {
        Task<IBankAccountVerificationResponse> VerifyBankAccount(string entityType, string entityId, IVerifyBankAccountRequest request);

        
    }
}