﻿using LendFoundry.Syndication.Giact.Proxy;
using LendFoundry.Syndication.Giact.Proxy.PostInquiryResponse;
using System;

namespace LendFoundry.Syndication.Giact.Tests
{
    public class FakeGiactProxy : IGiactProxy
    {
        private static InquiryResponse GetBankAccountResponse1()
        {
           return  new InquiryResponse()
            {
                PostInquiryResult = new PostInquiryResult
                {
                    AccountAddedDate = Convert.ToDateTime("05/01/2016"),
          
            AccountLastUpdatedDate = Convert.ToDateTime("07/01/2016"),
            AccountResponseCode = ServiceReference.AccountResponseCode.GN01.ToString(),
            BankName = "ABC Bank",
                    CreatedDate = Convert.ToDateTime("05/01/2016"),
                    CustomerResponseCode = ServiceReference.CustomerResponseCode.CA11.ToString(),
                    ItemReferenceId="12345",
                    VerificationResponse = ServiceReference.VerificationResponse_5_7.AcceptWithRisk.ToString()
        }
            };
           
          
            
          
        }
        private static InquiryResponse GetResponse2()
        {
            return new InquiryResponse()
            {
                PostInquiryResult = new PostInquiryResult
                {
                  
           
            AccountLastUpdatedDate = null,
            AccountResponseCode = ServiceReference.AccountResponseCode.GS01.ToString(),
            BankName = null,
                    ItemReferenceId = "12345",
                    CreatedDate = Convert.ToDateTime("05/01/2016"),
            CustomerResponseCode = ServiceReference.CustomerResponseCode.CA01.ToString(),
        
            VerificationResponse = ServiceReference.VerificationResponse_5_7.AcceptWithRisk.ToString()            
        }
            };
          
        }
        public InquiryResponse PostInquiry(Proxy.PostInquiryRequest.InquiryRequest inquiryRequest)
        {
            if (inquiryRequest.Inquiry.Check.AccountNumber == "12345")
                return GetBankAccountResponse1();
            if (inquiryRequest.Inquiry.Check.AccountNumber == "67890")
                return GetResponse2();
            return null;
        }
    }
}
