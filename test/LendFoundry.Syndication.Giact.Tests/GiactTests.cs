﻿using LendFoundry.Syndication.Giact.Proxy;
using System;
using Xunit;
using Moq;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Syndication.Giact.Request;
using LendFoundry.Syndication.Giact.ServiceReference;
using LendFoundry.Foundation.Services;

namespace LendFoundry.Syndication.Giact.Tests
{
    public class GiactTests
    {
        public static IVerifyBankAccountRequest GetPersonalGiactGoodRequest()
        {
            IVerifyBankAccountRequest request = new VerifyBankAccountRequest();
            request.AccountNumber = "0000000016";
            request.RoutingNumber = "122105278";
            request.AccountType = BankAccountType.Checking;
            request.FirstName = "Jane";
            request.LastName = "Smith";
            return request;
        }
        public static IVerifyBankAccountRequest GetBusinessGiactGoodRequest()
        {
            IVerifyBankAccountRequest request = new VerifyBankAccountRequest();
            request.AccountNumber = "0000000016";
            request.RoutingNumber = "122105278";
            request.AccountType = BankAccountType.Checking;
            request.BusinessName = "Jane";
           
            return request;
        }
        public static IGiactConfiguration GoodConfiguration
        {
            get
            {
                IGiactConfiguration configuration = new GiactConfiguration();
                configuration.ProxyUrl = "lendingusa.dev.lendfoundry.com:5049";
                configuration.BaseUrl = "https://sandbox.api.giact.com/verificationservices/v5/InquiriesWS-5-7.asmx";
                configuration.UserName = "B1PX-JVUBPU-W195J-EWYZS";
                configuration.Password = "_wbp0mjC-low";
                return configuration;
            }
        }
        public static IGiactConfiguration BrokenConfiguration { get; private set; } = new GiactConfiguration
        {
            UserName = "B1PX-JVUBPU-W195J-EWYZS",
            Password = "HO HAA HAA"
        };
        [Fact(Skip ="LiveTest")]
        public void BankAccountVerificationLiveTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new System.Collections.Generic.Dictionary<string, string> { {"merchant", "merchant" } });
            IGiactService service = new GiactService(new GiactProxy(GoodConfiguration), new GiactConfiguration(GoodConfiguration), mockLookup.Object);
            var result = service.VerifyBankAccount("merchant", "mer001", GetBusinessGiactGoodRequest()).Result;
            Assert.NotNull(result);            
            Assert.NotNull(result.CustomerResponseCode);
            Assert.NotNull(result.AccountResponseCode);
        }
        [Fact]
        public void GiactConfigurationNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
            Assert.Throws<ArgumentNullException>(() =>
                new GiactService(new GiactProxy(GoodConfiguration), null, mockLookup.Object));
        }
        [Fact]
        public void GiactConfigurationProxyUrlNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
            Assert.Throws<ArgumentNullException>(() =>
                new GiactService(new GiactProxy(new GiactConfiguration { BaseUrl = "https://sandbox.api.giact.com/verificationservices/v5/InquiriesWS-5-7.asmx", UserName = "B1PX-JVUBPU-W195J-EWYZS", Password = "_wbp0mjC-low" }), new GiactConfiguration { BaseUrl= "https://sandbox.api.giact.com/verificationservices/v5/InquiriesWS-5-7.asmx", UserName= "B1PX-JVUBPU-W195J-EWYZS", Password = "_wbp0mjC-low" }, mockLookup.Object));
        }
        [Fact]
        public void GiactConfigurationBaseUrlNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
            Assert.Throws<ArgumentNullException>(() =>
                new GiactService(new GiactProxy(new GiactConfiguration { ProxyUrl = "lendingusa.dev.lendfoundry.com:5049", UserName = "B1PX-JVUBPU-W195J-EWYZS", Password = "_wbp0mjC-low" }), new GiactConfiguration { ProxyUrl = "lendingusa.dev.lendfoundry.com:5049", UserName = "B1PX-JVUBPU-W195J-EWYZS", Password = "_wbp0mjC-low" }, mockLookup.Object));
        }
        [Fact]
        public void GiactConfigurationUsernameNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
            Assert.Throws<ArgumentNullException>(() =>
                new GiactService(new GiactProxy(new GiactConfiguration { BaseUrl = "https://sandbox.api.giact.com/verificationservices/v5/InquiriesWS-5-7.asmx", ProxyUrl = "lendingusa.dev.lendfoundry.com:5049", Password = "_wbp0mjC-low" }), new GiactConfiguration { BaseUrl = "https://sandbox.api.giact.com/verificationservices/v5/InquiriesWS-5-7.asmx", ProxyUrl = "lendingusa.dev.lendfoundry.com:5049", Password = "_wbp0mjC-low" }, mockLookup.Object));
        }
        [Fact]
        public void GiactConfigurationPasswordNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
            Assert.Throws<ArgumentNullException>(() =>
                new GiactService(new GiactProxy(new GiactConfiguration { BaseUrl = "https://sandbox.api.giact.com/verificationservices/v5/InquiriesWS-5-7.asmx", ProxyUrl = "lendingusa.dev.lendfoundry.com:5049", UserName = "B1PX-JVUBPU-W195J-EWYZS" }), new GiactConfiguration { BaseUrl = "https://sandbox.api.giact.com/verificationservices/v5/InquiriesWS-5-7.asmx", ProxyUrl = "lendingusa.dev.lendfoundry.com:5049", UserName = "B1PX-JVUBPU-W195J-EWYZS" }, mockLookup.Object));
        }
        [Fact]
        public void GiactLookupNullTest()
        {
           
            Assert.Throws<ArgumentNullException>(() =>
                new GiactService(new GiactProxy(GoodConfiguration), new GiactConfiguration (GoodConfiguration),null));
        }
        [Fact]
        public void GiactLookupWithEntityIdNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
            Assert.Throws<ArgumentException>(() =>
            {
                IGiactService service = new GiactService(new GiactProxy(GoodConfiguration), new GiactConfiguration(GoodConfiguration), mockLookup.Object);
                var result = service.VerifyBankAccount("merchant", null, GetBusinessGiactGoodRequest());
            });
        }
        [Fact]
        public void GiactLookupWithEntityTypeNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
            Assert.Throws<InvalidArgumentException>(() =>
            {
                IGiactService service = new GiactService(new GiactProxy(GoodConfiguration), new GiactConfiguration(GoodConfiguration), mockLookup.Object);
                var result = service.VerifyBankAccount(null, "mer001", GetBusinessGiactGoodRequest());
            });
        }
        [Fact]
        public void GiactLookupWithInvalidEntityTypeTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry("application", "application")).Returns(new System.Collections.Generic.Dictionary<string, string> { { "application","application"} });
            Assert.Throws<InvalidArgumentException>(() =>
            {
                IGiactService service = new GiactService(new GiactProxy(GoodConfiguration), new GiactConfiguration(GoodConfiguration), mockLookup.Object);
                var result = service.VerifyBankAccount("invalid", "mer001", GetBusinessGiactGoodRequest());
            });
        }
        [Fact]
        public void ClientNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
            Assert.Throws<ArgumentNullException>(() =>
            new GiactService(null, new GiactConfiguration(GoodConfiguration),mockLookup.Object));
        }
        [Fact]
        public void BankAccountRequestNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new System.Collections.Generic.Dictionary<string, string> { { "merchant", "merchant" } }); 
            Assert.Throws<ArgumentNullException>(() =>
            {
                IGiactService service = new GiactService(new GiactProxy(GoodConfiguration), new GiactConfiguration(GoodConfiguration),mockLookup.Object);
                var result = service.VerifyBankAccount("merchant","mer001",null);
            });
        }
        [Fact]
        public void BankAccountRequestWithAccountNumberNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new System.Collections.Generic.Dictionary<string, string> { { "merchant", "merchant" } });
            Assert.Throws<ArgumentException>(() =>
            {
                IGiactService service = new GiactService(new GiactProxy(GoodConfiguration), new GiactConfiguration(GoodConfiguration), mockLookup.Object);
                var result = service.VerifyBankAccount("merchant", "mer001", new VerifyBankAccountRequest { AccountType=BankAccountType.Checking,RoutingNumber="234",BusinessName="james" });
            });
        }
        [Fact]
        public void BankAccountRequestWithRoutingNumberNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new System.Collections.Generic.Dictionary<string, string> { { "merchant", "merchant" } });
            Assert.Throws<ArgumentException>(() =>
            {
                IGiactService service = new GiactService(new GiactProxy(GoodConfiguration), new GiactConfiguration(GoodConfiguration), mockLookup.Object);
                var result = service.VerifyBankAccount("merchant", "mer001", new VerifyBankAccountRequest { AccountType = BankAccountType.Checking, AccountNumber = "234", BusinessName="james" });
            });
        }
        [Fact]
        public void BankAccountRequestWithAccountAndRoutingNumberShouldNotSameTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new System.Collections.Generic.Dictionary<string, string> { { "merchant", "merchant" } });
            Assert.Throws<ArgumentException>(() =>
            {
                IGiactService service = new GiactService(new GiactProxy(GoodConfiguration), new GiactConfiguration(GoodConfiguration), mockLookup.Object);
                var result = service.VerifyBankAccount("merchant", "mer001", new VerifyBankAccountRequest { AccountType = BankAccountType.Checking, RoutingNumber = "234", AccountNumber = "234", BusinessName="james" });
            });
        }
        [Fact]
        public void BankAccountRequestWithBusinessNameNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new System.Collections.Generic.Dictionary<string, string> { { "merchant", "merchant" } });
            Assert.Throws<ArgumentException>(() =>
            {
                IGiactService service = new GiactService(new GiactProxy(GoodConfiguration), new GiactConfiguration(GoodConfiguration), mockLookup.Object);
                var result = service.VerifyBankAccount("merchant", "mer001", new VerifyBankAccountRequest { AccountType = BankAccountType.Checking, AccountNumber = "234" });
            });
        }
        [Fact]
        public void BankAccountRequestWithFirstNameNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new System.Collections.Generic.Dictionary<string, string> { { "application", "application" } });
            Assert.Throws<ArgumentException>(() =>
            {
                IGiactService service = new GiactService(new GiactProxy(GoodConfiguration), new GiactConfiguration(GoodConfiguration), mockLookup.Object);
                var result = service.VerifyBankAccount("application", "app001", new VerifyBankAccountRequest { AccountType = BankAccountType.Checking, AccountNumber = "234", LastName = "123" });
            });
        }
        [Fact]
        public void BankAccountRequestWithLastNameNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new System.Collections.Generic.Dictionary<string, string> { { "application", "application" } });
            Assert.Throws<ArgumentException>(() =>
            {
                IGiactService service = new GiactService(new GiactProxy(GoodConfiguration), new GiactConfiguration(GoodConfiguration), mockLookup.Object);
                var result = service.VerifyBankAccount("application", "app001", new VerifyBankAccountRequest { AccountType = BankAccountType.Checking, AccountNumber = "234", FirstName = "123" });
            });
        }
        [Fact]
        public void GiactMappingTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new System.Collections.Generic.Dictionary<string, string> { { "merchant", "merchant" } });
            IVerifyBankAccountRequest request = GetBusinessGiactGoodRequest();
            request.AccountNumber = "12345";
            IGiactService service = new GiactService(new FakeGiactProxy(), new GiactConfiguration(GoodConfiguration), mockLookup.Object);
            var result = service.VerifyBankAccount("merchant","mer001",request).Result;
            Assert.NotNull(result);
            
            
            Assert.Equal(Convert.ToDateTime("05/01/2016"), result.AccountAddedDate);
            Assert.Null(result.AccountClosedDate);
            Assert.Equal(Convert.ToDateTime("07/01/2016"), result.AccountLastUpdatedDate);
            Assert.Equal(AccountResponseCode.GN01.ToString(), result.AccountResponseCode);
            Assert.Equal("ABC Bank", result.BankName);
           
            Assert.Equal(Convert.ToDateTime("05/01/2016"), result.CreatedDate);
            Assert.Equal(CustomerResponseCode.CA11.ToString(), result.CustomerResponseCode);
           
            Assert.Equal("12345", result.ItemReferenceId);
          
        }
        [Fact]
        public void GiactMappingWithNullTest()
        {
            var mockLookup = new Mock<ILookupService>();
            mockLookup.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new System.Collections.Generic.Dictionary<string, string> { { "merchant", "merchant" } });
            IVerifyBankAccountRequest request = GetBusinessGiactGoodRequest();
            request.AccountNumber = "67890";
            IGiactService service = new GiactService(new FakeGiactProxy(), new GiactConfiguration(GoodConfiguration),mockLookup.Object);
            var result = service.VerifyBankAccount("merchant","mer001",request).Result;
            Assert.NotNull(result);            
            Assert.Null(result.AccountAddedDate);
            Assert.Null(result.AccountClosedDate);
            Assert.Null(result.AccountLastUpdatedDate);
            Assert.Equal(AccountResponseCode.GS01.ToString(), result.AccountResponseCode);
            Assert.Null(result.BankName);
           
            Assert.Equal(Convert.ToDateTime("05/01/2016"), result.CreatedDate);
            Assert.Equal(CustomerResponseCode.CA01.ToString(), result.CustomerResponseCode);
          
            Assert.Equal("12345", result.ItemReferenceId);
           
        }
    }
}
